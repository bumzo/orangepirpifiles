import datetime
import json
import paho.mqtt.client as mqtt
import schedule
import time
import os
import threading
import subprocess

file_path = 'schedule1.txt'

def on_connect(client, userdata, flags, rc):
    print("Orange Pi : CronJob : Connected with result code scheduled surviellance" + str(rc))
    #client.disconnect()
    #client.loop_stop()

def on_message(client, userdata, msg):
    msg.payload = msg.payload.decode("utf-8")
    if (msg.topic == 'ClearSchedule' and json.loads(msg.payload) == 1):
        try:
            os.remove(file_path)
        except Exception as e:
            print(e)
        schedule.cancel_job(job)
        schedule.clear('schedule')
        # print('qq')

def connectMqtt(client):
    # The callback for when the client receives a CONNACK response from the server.
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    # The callback for when a PUBLISH message is received from the server.
    client.on_connect = on_connect
    client.on_message = on_message
    # client.username_pw_set("admin", "HN9yx3OtGDZH")
    print("Orange Pi : CronJob : Connecting MQTT")
    client.connect("127.0.0.1")
    client.subscribe("ClearSchedule")
    client.loop_forever()

def job():
    global fileScheduleData
    global file_path
    client.publish("HDMIVideo", fileScheduleData['IP'], 0, False)
    client.publish("Mission",
                   json.dumps(fileScheduleData['dataPoints'], default=lambda o: o.__dict__, sort_keys=True, indent=4),
                   0, False)
    # while not os.path.exists(file_path):
    #     time.sleep(1)
    #
    # if os.path.isfile(file_path):
    #     fileRead = open(file_path, "r")
    # else:
    #     raise ValueError("Orange Pi : CronJob : %s isn't a file!" % file_path)
    #
    # fileScheduleData = json.loads(fileRead.read())
    # schedule.cancel_job(job)
    # print("Orange Pi : CronJob : Scheduled seconds ", fileScheduleData['timeInterval'])
    # schedule.every(fileScheduleData['timeInterval']).seconds.do(job)




client = mqtt.Client("OrangePiCronJob")
pthread = threading.Thread(target=connectMqtt, args=(client,))
pthread.start()
#connectMqtt(client)
'''
while not os.path.exists(file_path):
    time.sleep(1)

if os.path.isfile(file_path):
    fileRead = open(file_path, "r")
else:
    raise ValueError("Orange Pi : CronJob : %s isn't a file!" % file_path)

fileScheduleData = json.loads(fileRead.read())
print('Orange Pi : CronJob : Schedule Surveillance after seconds : ',fileScheduleData['timeInterval'])
# timeInterval
# dataPoints
schedule.every(fileScheduleData['timeInterval']).seconds.do(job)
'''

scheduleFlagFirst = True
while 1:
    schedule.run_pending()
    if os.path.isfile(file_path):
        if scheduleFlagFirst:
            fileRead = open(file_path, "r")
            time.sleep(0.01)
            fileScheduleData = json.loads(fileRead.read())
            fileRead.close()
            time.sleep(0.01)
            schedule.every(fileScheduleData['timeInterval']).seconds.do(job).tag('schedule')
            scheduleFlagFirst = False
        timeLeft = datetime.timedelta(seconds=int(schedule.idle_seconds()))
        client.publish("TimeLeft", str(timeLeft))
    else:
        scheduleFlagFirst = True
    try:
        output = subprocess.check_output("iwgetid -r", shell=True)
        if output.decode('ascii').strip() == 'nibrus_tech':
            outputin = subprocess.check_output("sudo nmcli dev wifi list | awk '/\*/{if (NR!=1) {print $7}}'", shell=True)
            client.publish("WifiStrength", str(outputin.decode().strip()))
        else:
            client.publish("WifiStrength", "Connection Lost")
            os.system("sudo nmcli networking on")
            os.system("sudo nmcli dev wifi connect nibrus_tech password @nibrus2#")
    except:
        client.publish("WifiStrength", "Connection Lost")
        os.system("sudo nmcli networking on")
        os.system("sudo nmcli dev wifi connect nibrus_tech password @nibrus2#")

    time.sleep(1)

