#!/bin/bash
# A simple script
killall -2 ffmpeg
current_time=$(date "+%Y.%m.%d-%H.%M.%S")
filename=Video.mp4
newfilename=$current_time.$filename
ffmpeg -f v4l2 -s 480x360 -r 60 -i /dev/video0 -g 60  -vb 800000 -an -f mpegts - | ffmpeg -f mpegts -i - -vcodec copy -f mpegts udp://"$1":12345 -vcodec copy -f mpegts "$newfilename"