import json
import paramiko
import paho.mqtt.client as mqtt
import subprocess
import threading


def on_connect(client, userdata, flags, rc):
    print("Orange Pi : Connected with result code " + str(rc))


def streamVideo(payload):
    rc = subprocess.call("./startVid.sh " + payload, shell=True)


def killStream():
    rc = subprocess.call("./killFFMPEG.sh", shell=True)


def on_message(client, userdata, msg):
    #print("Orange Pi : 1")
    #print(msg.topic)
    msg.payload = msg.payload.decode("utf-8")
    #print('Orange Pi : ', msg.payload)
    #print('Orange Pi : started...')
    if (msg.topic == 'HDMIVideo'):
        if (msg.payload != "0"):
            threading.Thread(target=streamVideo, args=(msg.payload,)).start()
        else:
            killStream()

    if (msg.topic == 'ScheduledSurvMessage'):
        try:
            file = open("schedule1.txt", "w")
            file.write(msg.payload)
            file.close()
        except:
            print("Orange Pi : Error writing file")


def connectMqtt(client):
    # The callback for when the client receives a CONNACK response from the server.
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    # The callback for when a PUBLISH message is received from the server.
    client.on_connect = on_connect
    client.on_message = on_message
    # client.username_pw_set("admin", "HN9yx3OtGDZH")
    client.connect("127.0.0.1")
    client.subscribe("HDMIVideo")
    client.subscribe("ScheduledSurvMessage")
    client.loop_forever()


client = mqtt.Client("OrangePi")
cameraThread = threading.Thread(target=connectMqtt, args=(client,))
cameraThread.start()
ssh = paramiko.SSHClient()
ssh.load_system_host_keys()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect('192.168.0.10', username='pi', password='jivan@123')
stdin, stdout, stderr = ssh.exec_command('python3 FinalDemo/558_174_DiffTwoIntegrated.py',get_pty=True)
#stdin, stdout, stderr = ssh.exec_command('python3 FinalDemo/558_23_DiffIDTwoIntegrated.py',get_pty=True)
for line in iter(stdout.readline, ""):
    print(line, end="")
print('Orange Pi : finished.')

# connectMqtt(client)
# ssh = paramiko.SSHClient()
# ssh.load_system_host_keys()
# ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
# try:
#   ssh.connect('192.168.0.10', username='pi', password='jivan@123')
#  print('started...')
# stdin, stdout, stderr = ssh.exec_command('python3 Desktop/mayank/FinalizingEveryThing/main1_new.py', get_pty=True)

# for line in iter(stdout.readline, ""):
#   print(line, end="")
# print('finished.')
# except:
#   print("Error occured connecting RPI")
