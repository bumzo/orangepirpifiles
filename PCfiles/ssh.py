import paramiko
import sys
import signal


def signal_handler(sig, frame):
    print('PC : You pressed Ctrl+C!')
    stdin, stdout, stderr = ssh.exec_command('./killFFMPEG.sh', get_pty=True)
    print('PC : Trying to close VideoStream')
    for line in iter(stdout.readline, ""):
        print(line, end="")
    print('PC : Windows : VideoStream closed')
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)
ssh = paramiko.SSHClient()
ssh.load_system_host_keys()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect('10.42.0.12', username='firefly', password='firefly')
print('PC : started...')
stdin, stdout, stderr = ssh.exec_command('python3 sshOrangePi.py', get_pty=True)

for line in iter(stdout.readline, ""):
    print(line, end="")
print('PC : finished.')
