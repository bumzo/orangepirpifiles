from __future__ import print_function
# import urllib2
# from pexpect import pxssh
import csv
import os
import subprocess
from array import *
# from serial import Serial
import time
import math
import sys
import threading

import cv2
import geopy.distance
import argparse
from datetime import datetime

import numpy as np
from dronekit import connect, VehicleMode, LocationGlobal, LocationGlobalRelative
from pymavlink import mavutil  # Needed for command message definitions
import time
import math
import paho.mqtt.client as mqtt
import json
import utm
from goprocam import GoProCamera
from goprocam import constants

# import Camera_video_stream
# import cameraControl
# from xiaomi_yi import XiaomiYi
import time

from cv2 import aruco
from picamera.array import PiRGBArray
from picamera import PiCamera
import pid
import os
import datetime

connection_string = '/dev/ttyAMA0'
vehicle = connect(connection_string, baud=921600, wait_ready=True)
firstImage2Download = 0
count = 0
ncount = 0
_FINISH = True
_hover = False
_lastPoint = False
missionpath = ""
totalDistanceTravelled = 0
batteryInitially = ''
timeInitial = time.time()
metaData = ''
_isPhoto = True

client = mqtt.Client("RaspberryPi")
# .write(str(99) + '\n')
mqttIP = '192.168.0.1'
sitl = None
emergencyLanding = False
logList = []
flagLastLocation = 0
distance = 0

highResStream = None
lowResStream = None
highResCap = None
lowResCap = None
camera_vision = None

gpCam = None
surveillanceFlag = False
if not os.path.exists('logs'):
    os.makedirs('logs')


def getFirstImage(lastImageName):
    global firstImage2Download
    x = lastImageName.split("YDXJ")
    intLastName = int(x[1].split(".")[0])
    firstImage2Download = intLastName + 1
    # intLastName + 1
    print("first image is")
    print(firstImage2Download)


# def validateImage():
#     return lastImageName


def set_velocity_body(vx, vy, vz):
    """ Remember: vz is positive downward!!!
    http://ardupilot.org/dev/docs/copter-commands-in-guided-mode.html

    Bitmask to indicate which dimensions should be ignored by the vehicle
    (a value of 0b0000000000000000 or 0b0000001000000000 indicates that
    none of the setpoint dimensions should be ignored). Mapping:
    bit 1: x,  bit 2: y,  bit 3: z,
    bit 4: vx, bit 5: vy, bit 6: vz,
    bit 7: ax, bit 8: ay, bit 9:


    """
    msg = vehicle.message_factory.set_position_target_local_ned_encode(
        0,  # time_boot_ms (not used)
        0, 0,  # target system, target component
        mavutil.mavlink.MAV_FRAME_LOCAL_NED,  # frame
        0b0000111111000111,  # type_mask (only speeds enabled)
        0, 0, 0,  # x, y, z positions (not used)
        vx, vy, vz,  # x, y, z velocity in m/s
        0, 0, 0,  # x, y, z acceleration (not supported yet, ignored in GCS_Mavlink)
        0, 0)  # yaw, yaw_rate (not supported yet, ignored in GCS_Mavlink)
    vehicle.send_mavlink(msg)
    vehicle.flush()


def set_velocity_body_v(vx, vy, vz):
    """ Remember: vz is positive downward!!!
    http://ardupilot.org/dev/docs/copter-commands-in-guided-mode.html

    Bitmask to indicate which dimensions should be ignored by the vehicle
    (a value of 0b0000000000000000 or 0b0000001000000000 indicates that
    none of the setpoint dimensions should be ignored). Mapping:
    bit 1: x,  bit 2: y,  bit 3: z,
    bit 4: vx, bit 5: vy, bit 6: vz,
    bit 7: ax, bit 8: ay, bit 9:


    """
    msg = vehicle.message_factory.set_position_target_local_ned_encode(
        0,  # time_boot_ms (not used)
        0, 0,  # target system, target component
        mavutil.mavlink.MAV_FRAME_BODY_OFFSET_NED,  # frame
        0b0000111111000111,  # type_mask (only speeds enabled)
        0, 0, 0,  # x, y, z positions (not used)
        vx, vy, vz,  # x, y, z velocity in m/s
        0, 0, 0,  # x, y, z acceleration (not supported yet, ignored in GCS_Mavlink)
        0, 0)  # yaw, yaw_rate (not supported yet, ignored in GCS_Mavlink)
    vehicle.send_mavlink(msg)
    vehicle.flush()


def get_distance_metres(aLocation1, aLocation2):
    """
    Returns the ground distance in metres between two LocationGlobal objects.

    This method is an approximation, and will not be accurate over large distances and close to the
    earth's poles. It comes from the ArduPilot test code:
    https://github.com/diydrones/ardupilot/blob/master/Tools/autotest/common.py
    """
    dlat = aLocation2.lat - aLocation1.lat
    dlong = aLocation2.lon - aLocation1.lon
    # return (math.sqrt((dlat*dlat) + (dlong*dlong)) * 1.113195e5)
    return geopy.distance.vincenty((aLocation1.lat, aLocation1.lon), (aLocation2.lat, aLocation2.lon)).m


def goto(targetLocation, previousLocation, centerLocation=None, isFirst=False, isVideoTaken=0):
    """
    Moves the vehicle to a position dNorth metres North and dEast metres East of the current position.

    The method takes a function pointer argument with a single `dronekit.lib.LocationGlobal` parameter for
    the target position. This allows it to be called with different position-setting commands.
    By default it uses the standard method: dronekit.lib.Vehicle.simple_goto().

    The method reports the distance to target every two seconds.
    """
    global _hover
    global totalDistanceTravelled
    currentLocation = vehicle.location.global_relative_frame
    print("RaspberryPi : Location at the begining")
    print('RaspberryPi : ', currentLocation)
    waitCounter = array('d', [0.00, 0.00, 0.00, 0.00, 0.00])
    vehicle.airspeed = 10
    vehicle.groundspeed = 10
    loopCounter = 0
    targetDistance = get_distance_metres(currentLocation, targetLocation)
    totalDistanceTravelled = totalDistanceTravelled + targetDistance
    const = 180 / math.pi
    s = utm.from_latlon(targetLocation.lat, targetLocation.lon)
    theta = math.acos(s[0] / math.sqrt((s[0] * s[0]) + (s[1] * s[1]))) * const
    if (s[0] < 0 and s[1] > 0):
        theta = 180 - theta
    if (s[0] < 0 and s[1] < 0):
        theta = 180 + theta
    if (s[0] > 0 and s[1] < 0):
        theta = 360 - theta
    # condition_yaw(vehicle, theta)
    # time.sleep(2)
    print('RaspberryPi : ', vehicle.mode.name);
    while vehicle.mode.name == "GUIDED":  # Stop action if we are no longer in guided mode.
        if (not (_hover)):
            vehicle.simple_goto(targetLocation)
            if centerLocation is not None:
                set_roi(centerLocation)
            remainingDistance = get_distance_metres(vehicle.location.global_relative_frame, targetLocation)
            if ((targetDistance - remainingDistance) > 5 and (isFirst == False) and (isVideoTaken == 0)):
                targetDistance = remainingDistance
                # camera()
                cameraThread = threading.Thread(target=camera)
                cameraThread.start()
            if remainingDistance <= 0.5:  # Just below target, in case of undershoot.
                client.publish("DroneAutomatorReply", "Reached target")
                break
            time.sleep(1)
            itrCycle = loopCounter % 4
            if (loopCounter > 0 and itrCycle == 0):
                if ((abs(waitCounter[0] - waitCounter[3]) < 0.3) and remainingDistance < 1):
                    diffX = targetLocation.lon - vehicle.location.global_relative_frame.lon
                    diffY = targetLocation.lat - vehicle.location.global_relative_frame.lat
                    set_velocity_body(diffY * 1.113195e5, diffX * 1.113195e5, 0)
                    loopCounter = -1
                    client.publish("DroneAutomatorReply", "Setting velocity for goto")
                    time.sleep(2)
                    client.publish("DroneAutomatorReply", "Breaking as approx target is reached")
                    break

            waitCounter[itrCycle] = remainingDistance
            loopCounter = loopCounter + 1
        else:
            print("RaspberryPi : hovering")
            set_velocity_body(0, 0, 0)
            time.sleep(1)


def goto_position_target_global_int(aLocation, vz=0):
    """
    Send SET_POSITION_TARGET_GLOBAL_INT command to request the vehicle fly to a specified LocationGlobal.

    For more information see: https://pixhawk.ethz.ch/mavlink/#SET_POSITION_TARGET_GLOBAL_INT

    See the above link for information on the type_mask (0=enable, 1=ignore).
    At time of writing, acceleration and yaw bits are ignored.
    """
    msg = vehicle.message_factory.set_position_target_global_int_encode(
        0,  # time_boot_ms (not used)
        0, 0,  # target system, target component
        mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT_INT,  # frame
        0b0000111111111000,  # type_mask (only speeds enabled)
        aLocation.lat * 1e7,  # lat_int - X Position in WGS84 frame in 1e7 * meters
        aLocation.lon * 1e7,  # lon_int - Y Position in WGS84 frame in 1e7 * meters
        aLocation.alt,
        # alt - Altitude in meters in AMSL altitude, not WGS84 if absolute or relative, above terrain if GLOBAL_TERRAIN_ALT_INT
        0,  # X velocity in NED frame in m/s
        0,  # Y velocity in NED frame in m/s
        0,  # Z velocity in NED frame in m/s
        0, 0, 0,  # afx, afy, afz acceleration (not supported yet, ignored in GCS_Mavlink)
        0, 0)  # yaw, yaw_rate (not supported yet, ignored in GCS_Mavlink)
    # send command to vehicle
    vehicle.send_mavlink(msg)


def advanced_takeoff_globalframe(location, targetAlt=None):
    if targetAlt is not None:
        altitude = float(targetAlt)
        if math.isnan(altitude) or math.isinf(altitude):
            raise ValueError("Altitude was NaN or Infinity. Please provide a real number")
        msg = vehicle.message_factory.command_long_encode(
            0, 0,  # target system, target component
            mavutil.mavlink.MAV_CMD_NAV_TAKEOFF,  # command
            0,  # confirmation
            0,  # Minimum pitch (if airspeed sensor present), desired pitch without sensor [rad]
            0, 0,  # Empty
            0,  # Yaw angle (if magnetometer present), ignored without magnetometer. NaN for unchanged
            location.lat,  # Latitude
            location.lon,  # Longitude
            altitude)  # target altitude in metres
    # send command to vehicle
    vehicle.send_mavlink(msg)


def arm_and_takeoff(aTargetAltitude):
    print("RaspberryPi : Basic pre-arm checks")
    # Don't let the user try to arm until autopilot is ready
    timeN = 1
    while not vehicle.is_armable:
        print("RaspberryPi : Waiting for vehicle to initialise...")
        time.sleep(1)
        timeN = timeN + 1
    #   if timeN >10:#
    # connection_string = '/dev/ttyACM0'
    #   sitl = None
    #   vehicle = connect(connection_string, wait_ready=True)
    #   ChangeMode(vehicle, "GUIDED")
    #   timeN = 1
    print("RaspberryPi : Arming motors")
    # Copter should arm in GUIDED mode
    vehicle.mode = VehicleMode("GUIDED")
    vehicle.armed = True
    cntArm = 0
    while not vehicle.armed:
        print("RaspberryPi : Waiting for arming...")
        cntArm = cntArm + 1
        time.sleep(1)
        if (cntArm >= 150):
            client.publish("DroneAutomate", "0")
    # sys.exit()

    print("RaspberryPi : Taking off!")
    location = vehicle.location.global_relative_frame
    print("RaspberryPi : location: %s" % location)
    k = 0

    while True:
        current_altitude = vehicle.location.global_relative_frame.alt
        print("RaspberryPi : Altitude: %f  Desired: %f" % (current_altitude, aTargetAltitude))
        if current_altitude >= aTargetAltitude * 0.95:  # Trigger just below target alt.
            print("RaspberryPi : Reached target altitude")
            break
        advanced_takeoff_globalframe(location, aTargetAltitude)  # Take off to target altitude
        time.sleep(1)
        later_altitude = vehicle.location.global_relative_frame.alt
        if (abs(later_altitude - current_altitude) < 0.3):
            k = k + 1
        else:
            k = 0
        if k >= 8:
            set_velocity_body(0, 0, -0.125)
            time.sleep(4)


def gotoAlt(aTargetAltitude):
    location = vehicle.location.global_relative_frame
    c = 1
    if (location.alt > aTargetAltitude):
        c = -1
    while location.alt < aTargetAltitude:
        set_velocity_body(0, 0, -0.25 * c)
        time.sleep(1)
        location = vehicle.location.global_relative_frame


def ChangeMode(vehicle, mode):
    if mode == "RTL":
        vehicle.parameters['RTL_ALT'] = 0
    # vehicle.parameters['RTL_ALT_FINAL'] = 0
    while vehicle.mode != VehicleMode(mode):
        print("RaspberryPi : waiting to set mode to %s..." % mode)
        vehicle.mode = VehicleMode(mode)
        time.sleep(2)
    return True


def condition_yaw(vehicle, heading, relative=False):
    """
    Send MAV_CMD_CONDITION_YAW message to point vehicle at a specified heading (in degrees).
    This method sets an absolute heading by default, but you can set the `relative` parameter
    to `True` to set yaw relative to the current yaw heading.

    By default the yaw of the vehicle will follow the direction of travel. After setting
    the yaw using this function there is no way to return to the default yaw "follow direction
    of travel" behaviour (https://github.com/diydrones/ardupilot/issues/2427)
    """

    if relative:
        is_relative = 1  # yaw relative to direction of travel
    else:
        is_relative = 0  # yaw is an absolute angle
    print('................. %0.f' % heading)
    # create the CONDITION_YAW command using command_long_encode()
    msg = vehicle.message_factory.command_long_encode(
        0, 0,  # target system, target component
        mavutil.mavlink.MAV_CMD_CONDITION_YAW,  # command
        0,  # confirmation
        heading,  # param 1, yaw in degrees
        0,  # param 2, yaw speed deg/s
        1,  # param 3, direction -1 ccw, 1 cw
        is_relative,  # param 4, relative offset 1, absolute angle 0
        0, 0, 0)  # param 5 ~ 7 not used
    # send command to vehicle
    vehicle.send_mavlink(msg)


def set_roi(location):
    # create the MAV_CMD_DO_SET_ROI command
    msg = vehicle.message_factory.command_long_encode(
        0, 0,  # target system, target component
        mavutil.mavlink.MAV_CMD_DO_SET_ROI,  # command
        0,  # confirmation
        0, 0, 0, 0,  # params 1-4
        location.lat,
        location.lon,
        location.alt
    )
    # send command to vehicle
    vehicle.send_mavlink(msg)


def cmdCircular(radius, angularVelocity):
    Vy = angularVelocity * radius
    yawRate = -angularVelocity
    bootTime = 0  # define startTime = time.time() at the begining of code
    msg = vehicle.message_factory.set_position_target_local_ned_encode(
        bootTime,
        0, 0,
        mavutil.mavlink.MAV_FRAME_BODY_OFFSET_NED,
        0b010111000111,
        0, 0, 0,
        0, Vy, 0,
        0, 0, 0,
        0, yawRate)
    vehicle.send_mavlink(msg)
    vehicle.flush()


@vehicle.on_attribute('armed')
def listener(self, name, message):
    global emergencyLanding
    global logList
    global flagLastLocation
    global totalDistanceTravelled
    global distance
    global _FINISH

    global highResStream
    global lowResStream
    global highResCap
    global lowResCap
    global camera_vision
    global gpCam
    global surveillanceFlag
    client.publish("DroneAutomatorReply", "SavingFile")
    timestr = time.strftime("%Y%m%d-%H%M%S")
    filename = 'logs/' + timestr + '.log'

    if not (message):
        _FINISH = True
        if surveillanceFlag:
            # gpCam.shutter(constants.stop)
            surveillanceFlag = False
        try:
            highResStream.close()
            lowResStream.close()
            highResCap.close()
            lowResCap.close()
            camera_vision.close()
        except:
            print("Camera close error")
        with open('%s' % filename, 'w+', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(['Distance', 'Battery', 'Current'])
            writer.writerows(logList)
            client.publish("HDMIVideo", "0")
        if not (emergencyLanding):
            client.publish("CompleteMission", "1")
        else:
            client.publish("CompleteMission", "0")

    flagLastLocation = 0
    logList = []
    distance = 0
    totalDistanceTravelled = 0


@vehicle.on_message('STATUSTEXT')
def listener(self, name, message):
    if message.text == 'GPS Glitch':
        client.publish("DroneAutomatorReply", "GPS Glitch")
    if message.text == 'GPS Glitch cleared':
        client.publish("DroneAutomatorReply", "GPS Glitch Cleared")
    if 'autotune' in message.text.lower():
        client.publish("Autotune", str(message.text))


def on_connect(client, userdata, flags, rc):
    print("RaspberryPi : Connected with result code " + str(rc))


def camera():
    global _isPhoto
    global _lastPoint
    global metaData
    global count
    global ncount
    global gpCam
    global firstImage2Download
    # camera = XiaomiYi(ip="192.168.42.1", port=7878, timeout=10)
    print("RaspberryPi : thread started")
    # stringURL = "https://tyaxphbs.p73.rt3.io/camera.php?imgName="
    # camera.connect()
    # time.sleep(5)
    # camera.connect()
    a = 0
    # camera.stream()
    if (True):
        if _lastPoint == False:
            try:
                gpCam.take_photo()
                print("RaspberryPi : Clicked")
            except:
                print("RaspberryPi : Error occured in clicking photo. Check if camera is connected properly")
            # time.sleep(2)
    # print("Video")
    # camera.start_video()
    else:
        if _lastPoint == False or a < 10:
            client.publish("DroneAutomatorReply", "Clicked a photo")
            location = vehicle.location.global_relative_frame
            cameraControl.takePhoto()
            print("taking photo")
            count = count + 1
            ncount = count
            imgtoD = str(firstImage2Download)
            print(imgtoD)
            if (len(imgtoD) == 3):
                imgtoD = "0" + imgtoD
            imgtoD = "YDXJ" + imgtoD + ".jpg"
            # while validateImage()!=imgtoD:
            client.publish("DroneAutomatorReply", "Validated Photo")
            print("insta")
            metaData = metaData + (imgtoD + "," + str(location.lat) + "," + str(location.lon) + "," + str(
                location.alt) + "," + str(vehicle.attitude.pitch) + "," + str(vehicle.attitude.roll) + "," + str(
                vehicle.attitude.yaw) + "\n")
            print(metaData)
            firstImage2Download = firstImage2Download + 1
            a = a + 1


def perform_goto(data):
    #   cameraThread = threading.Thread(target=camera)
    #        cameraThread.start()

    global _hover
    global _lastPoint
    global metaData
    global totalDistanceTravelled
    global timeInitial
    global batteryInitially
    global _FINISH
    global emergencyLanding
    global gpCam
    global surveillanceFlag
    timeInitial = int(time.time())
    batteryInitial = vehicle.battery
    _lastPoint = False
    _FINISH = False
    emergencyLanding = False
    #   arm_and_takeoff(10)
    strt = 0
    print("Check1")
    for i in data:
        if strt == 0:
            print("RaspberryPi : Calibrating pressure")
            master = mavutil.mavlink_connection(connection_string, baud=921600)
            master.calibrate_pressure()
            time.sleep(2)
            print("RaspberryPi : Pressure calibrated")
            vehicle.parameters['WP_YAW_BEHAVIOR'] = i[3]
            arm_and_takeoff(i[2])
            print('Raspberry Pi :Inside perform_goto')
        goto(LocationGlobalRelative(i[0], i[1], i[2]), LocationGlobalRelative(i[0], i[1], i[2]), None,
             True if strt == 0 else False, i[3])
        if strt == 0:
            # vehicle.parameters['WP_YAW_BEHAVIOR'] = 0  # Never change yaw
            # cameraThread = threading.Thread(target=camera)
            # cameraThread.start()
            strt = 1
        # if (False):
        #     _hover = True
        #     _lastPoint = False
        #     # cameraThread = threading.Thread(target=camera)
        #     # cameraThread.start()
        #     while False:
        #         print("hovering")
        #         set_velocity_body(0, 0, 0)
        #         time.sleep(1)
        # _lastPoint =True
        # else:
        #     p = i[3]
        # while False:
        #     print("RaspberryPi : hovering")
        #     set_velocity_body(0, 0, 0)
        #     time.sleep(1)
        #     p = p - 1
    _lastPoint = True

    # totalTimeTaken = str(int(time.time()) - timeInitial)
    # data2Write = [str(totalDistanceTravelled) + "," + totalTimeTaken + "," + str(batteryInitially) + "," + str(
    #     vehicle.battery.voltage)]
    # logList.append(data2Write)

    # ChangeMode(vehicle, "RTL")
    client.subscribe("HomePos")
    # if vehicle.mode.name == "GUIDED":
    #     client.publish("CompleteMission", "1")
    # time.sleep(20)
    if vehicle.mode.name != "LAND":
        ChangeMode(vehicle, "RTL")
    # Close vehicle object before exiting script
    # goto(LocationGlobalRelative(vehicle.home_location.lat, vehicle.home_location.lon,
    #                             vehicle.location.global_relative_frame.alt),
    #      LocationGlobalRelative(vehicle.home_location.lat, vehicle.home_location.lon,
    #                             vehicle.location.global_relative_frame.alt), None, False)
    #
    # goto(LocationGlobalRelative(vehicle.home_location.lat, vehicle.home_location.lon, 7),
    #      LocationGlobalRelative(vehicle.home_location.lat, vehicle.home_location.lon,
    #                             vehicle.location.global_relative_frame.alt), None, False)
    while vehicle.location.global_relative_frame.alt > 9.5 and vehicle.mode.name != "LAND":
        print(vehicle.location.global_relative_frame.alt)
        time.sleep(0.6)

    if vehicle.mode.name != "LAND":
        ChangeMode(vehicle, "GUIDED")
        visionLanding()
    # set_velocity_body(0.3,0,0)
    # time.sleep(3)
    print("RaspberryPi : Close vehicle object")

    # Shut down simulator if it was started.
    if sitl is not None:
        sitl.stop()
    print("RaspberryPi : Completed")


def visionLanding():
    global highResStream
    global lowResStream
    global highResCap
    global lowResCap
    global camera_vision

    try:
        camera_vision = PiCamera()
    except:
        print('Trying to close and open PiCam')
        camera_vision.close()
        camera_vision = PiCamera()

    camera_vision.resolution = (640, 480)
    camera_vision.framerate = 30

    highResCap = PiRGBArray(camera_vision)
    lowResCap = PiRGBArray(camera_vision, size=(320, 240))

    highResStream = camera_vision.capture_continuous(highResCap, format="bgr", use_video_port=True)
    lowResStream = camera_vision.capture_continuous(lowResCap, format="bgr", use_video_port=True, splitter_port=2,
                                                    resize=(320, 240))
    set_velocity_body_v(0, 0, 0.01)
    datetimeStampFolder = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    # mydirDetected = os.path.join(os.getcwd(), datetimeStampFolder + 'Detected')
    # if not os.path.exists(mydirDetected):
    #     os.makedirs(mydirDetected)
    time.sleep(1)
    print("RaspberryPi : Camera Done warming up")
    kp = 0.25
    ki = 0.2
    kd = 0
    ki_max = 50
    down_vel = 0.25
    bcir = 12
    scir = 14
    previousValueBig = []
    firstflagBig = 0

    previousValue = []
    firstflag = 0
    diffvalue = 100
    camera_matrix = np.load('/home/pi/bumzo/picam_mtx.npy')  # loading the camera matrix
    dist_coeffs = np.load('/home/pi/bumzo/picam_dist.npy')  # loading the distortion matrix
    image_size = (640, 480)  # same size to be kept which was kept during calibration

    x_pid = pid.pid(0.2, 0, 0, 30)
    y_pid = pid.pid(0.2, 0, 0, 30)

    x_pid_small = pid.pid(0.15, 0, 0, 30)
    y_pid_small = pid.pid(0.15, 0, 0, 30)

    bigmaxvel = 0.3
    smallmaxvel = 0.2
    # aruco_dict = aruco.Dictionary_get(aruco.DICT_ARUCO_ORIGINAL)
    # arucoParams = aruco.DetectorParameters_create()
    print('Starting detection now')
    first = True

    while vehicle.mode == "GUIDED":
        # pehla = time.time()
        hrs = next(highResStream)
        if (hrs is None):
            hrs = next(lowResStream)
            image_size = (320, 240)
        cv2_img = hrs.array
        frame = cv2.resize(cv2_img, image_size)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        aruco_dict = aruco.Dictionary_get(aruco.DICT_ARUCO_ORIGINAL)
        parameters = aruco.DetectorParameters_create()

        corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=parameters,
                                                              cameraMatrix=camera_matrix, distCoeff=dist_coeffs)
        Counter = 0
        sevenIDdetected = False
        sevenIndex = -1
        sixIDdetected = False
        sixIndex = -1
        if ids is not None:
            for i in range(len(ids)):
                if ids[i] == 7:
                    sevenIDdetected = True
                    sevenIndex = i
                if ids[i] == 6:
                    sixIDdetected = True
                    sixIndex = i

            if sevenIDdetected and vehicle.rangefinder.distance > 5:
                ret = aruco.estimatePoseSingleMarkers(corners=corners[sevenIndex], markerLength=37,
                                                      cameraMatrix=camera_matrix,
                                                      distCoeffs=dist_coeffs)

                rvec, tvec = ret[0][0, 0, :], ret[1][0, 0, :]

                x_cm = tvec[0]
                y_cm = tvec[1]
                z_cm = tvec[2]

                # if firstflagBig == 1:
                #     if abs((x_cm) - previousValueBig[0]) > diffvalue:
                #         x_cm = previousValueBig[0]
                #     if abs((y_cm) - previousValueBig[1]) > diffvalue:
                #         y_cm = previousValueBig[1]
                #         previousValueBig = [x_cm, y_cm, z_cm]
                #
                # if firstflagBig == 0:
                #     previousValueBig = [x_cm, y_cm, z_cm]
                #     firstflagBig = firstflagBig + 1

                xdash = y_cm
                ydash = -x_cm

                x_cm = xdash - 15
                y_cm = ydash + 12

                x_m = 0.01 * x_cm
                y_m = 0.01 * -y_cm

                vx = x_pid.get_pid(y_m, 0.2)
                vy = y_pid.get_pid(x_m, 0.2)

                if vx >= 0:
                    vx = min(vx, bigmaxvel)
                else:
                    vx = max(vx, -bigmaxvel)

                if vy >= 0:
                    vy = min(vy, bigmaxvel)
                else:
                    vy = max(vy, -bigmaxvel)

                if vehicle.rangefinder.distance > 5.5:
                    print("7 m ke upar hai, neeche aa raha hai")
                    set_velocity_body_v(vx, vy, 0.25)

                else:
                    if abs(x_cm) <= 15 and abs(y_cm) < 15:
                        print('doosre marker ki baari aa rahi hai')
                        set_velocity_body_v(0, 0, 0.25)
                        # str_position = "CAM POS x=%4.1f  y=%4.1f  z=%4.1f" % (x_cm, y_cm, z_cm)
                        # cv2.putText(frame, str_position, (0, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 255), 3)
                        # cv2.imwrite(mydirDetected + '/' + str(Counter) + '.jpg', frame)
                        Counter = Counter + 1
                    else:
                        print('Adjust ho raha hai bade circle pe')
                        set_velocity_body_v(vx, vy, 0)

                print("Bada X_cm", x_cm, "    Y_cm", y_cm, "    Height", vehicle.rangefinder.distance)

            elif sixIDdetected and vehicle.rangefinder.distance <= 5:
                # if delayInChotu:
                #     delayInChotu = False
                #     time.sleep(1)
                ret = aruco.estimatePoseSingleMarkers(corners=corners[sixIndex], markerLength=17.4,
                                                      cameraMatrix=camera_matrix,
                                                      distCoeffs=dist_coeffs)

                rvec, tvec = ret[0][0, 0, :], ret[1][0, 0, :]

                x_cm = tvec[0]
                y_cm = tvec[1]
                z_cm = tvec[2]

                # if firstflag == 1:
                #     if abs((x_cm) - previousValue[0]) > 100:
                #         x_cm = previousValue[0]
                #     if abs((y_cm) - previousValue[1]) > 100:
                #         y_cm = previousValue[1]
                #     previousValue = [x_cm, y_cm, z_cm]
                #
                # if firstflag == 0:
                #     previousValue = [x_cm, y_cm, z_cm]
                #     firstflag = firstflag + 1

                xdash = y_cm
                ydash = -x_cm

                x_cm = xdash - 16
                y_cm = ydash + 6

                x_m = 0.01 * x_cm
                y_m = 0.01 * -y_cm

                vx = x_pid_small.get_pid(y_m, 0.2)
                vy = y_pid_small.get_pid(x_m, 0.2)

                if vx >= 0:
                    vx = min(vx, smallmaxvel)
                else:
                    vx = max(vx, -smallmaxvel)

                if vy >= 0:
                    vy = min(vy, smallmaxvel)
                else:
                    vy = max(vy, -smallmaxvel)

                if vehicle.rangefinder.distance > 1.5:
                    print("Chota: Adjust hote hote neeche aa raha hai")
                    set_velocity_body_v(vx, vy, 0.2)

                else:
                    if abs(x_cm) < 15 and abs(y_cm) < 15:
                        set_velocity_body_v(0, 0, 0)
                        print("Chaning to LAND MODE normally")
                        vehicle.mode = VehicleMode("LAND")
                        time.sleep(1.5)
                        highResCap.truncate(0)
                        lowResCap.truncate(0)
                        highResStream.close()
                        lowResStream.close()
                        highResCap.close()
                        lowResCap.close()
                        camera_vision.close()
                        break
                    else:
                        set_velocity_body_v(vx, vy, 0)

                print("Chota X_cm", x_cm, "    Y_cm", y_cm, "    Height", vehicle.rangefinder.distance)

        if ids is None and vehicle.rangefinder.distance > 0.5 and vehicle.rangefinder.distance < 7:
            # cv2.imwrite(mydirNotDetected + '/' + str(NondetectCounter) + '.jpg', frame)
            # NondetectCounter = NondetectCounter + 1
            print("7 se neeche aur 1 se upar  me marker detect nahi hua hai, upar upar")
            set_velocity_body_v(0, 0, -0.1)
        try:
            if first:
                cv2.imwrite(datetimeStampFolder + '.jpg', frame)
                first = False
        except:
            print('Capturing error')
        highResCap.truncate(0)
        lowResCap.truncate(0)
        time.sleep(0.1)
        # print('time : ', time.time() - pehla)


def on_message(client, userdata, msg):
    global _hover
    global _isPhoto
    global _FINISH
    global missionpath
    global gpcam
    #client.publish("DroneAutomatorReply", msg.topic + " " + str(msg.payload))
    print("RaspberryPi : 1")
    print('RaspberryPi Topic: ', msg.topic)
    msg.payload = msg.payload.decode("utf-8")
    print('RaspberryPi MessagePayload: ', msg.payload)
    if (msg.topic == 'Mission') and _FINISH == True and not (vehicle.armed):
        try:
            data = json.loads(msg.payload)
            if data[0][3] == 0:
                print("Checking Camera Wifi connection")
                return_code = subprocess.call("/sbin/ifconfig wlan0 | grep inet", shell=True)
                if return_code == 0:
                    print("IP avaialble, trying to connect")
                    gpcam = GoProCamera.GoPro()
                else:
                    print("IP not avaiable")
                    subprocess.call("sudo /sbin/ifconfig wlan0 down", shell=True)
                    subprocess.call("sudo /sbin/ifconfig wlan0 up", shell=True)
                    print("Trying again for Camera connection")
                    time.sleep(3)
                    return_code = subprocess.call("/sbin/ifconfig wlan0 | grep inet", shell=True)
                    print(return_code)
                    if return_code == 0:
                        print("IP avaiable now, trying to connect")
                        gpcam = GoProCamera.GoPro()
                    else:
                        print("Camera not connected!!! Check WIFI connection")
                        client.publish("DroneAutomatorReply", "Camera not connected!!! Check WIFI connection")
                        return
            missionpath = str(data)
            print("RaspberryPi : Inside Mission Topic")
            print('RaspberryPi MissionMessagePayload : ', str(data))
            ChangeMode(vehicle, "GUIDED")
            _FINISH = False
            print('Raspberry Pi :Starting perform_goto function')
            pthread = threading.Thread(target=perform_goto, args=(data,))
            pthread.start()
        except:  # catch *all* exceptions
            e = sys.exc_info()[0]
            client.publish("DroneAutomatorReply", str(e))
    else:
        client.publish("DroneAutomatorReply", "Drone is already in a mission")

        # print("hello")
    if (msg.topic == "HomePos"):
        my_list = data.split(",")
        a = vehicle.location.global_frame
        a.lat = float(my_list[0])
        a.lon = float(my_list[1])

        msg = vehicle.message_factory.set_home_position_encode(
            0,  # System ID maybe
            a.lat * 1.0e7,  # lat - int32_t Latitude (WGS84), in degrees \* 1E7
            a.lon * 1.0e7,  # lon - int32_t Longitude (WGS84), in degrees \* 1E7
            a.alt - vehicle.location.global_relative_frame.alt,
            # alt - int32_t  Altitude (AMSL), in meters \* 1000 (positive for up)
            0, 0, 0, [1, 1, 1, 1], 0, 0, 0  # Remaining parameters not supported by ardupilot.
        )
        ChangeMode(vehicle, "RTL")
        unsubscribe(msg.topic)
    if (msg.topic == "PhotoVideo"):
        if (msg.payload == 0):
            _isPhoto = False
        else:
            _isPhoto = True
    if (msg.topic == "HoverMove"):
        if (json.loads(msg.payload) == 0):
            print("RaspberryPi : HoverTopic")
            _hover = True
        else:
            _hover = False
    if (msg.topic == 'RTL_LAND'):
        datartl = json.loads(msg.payload)
        global emergencyLanding
        emergencyLanding = True
        if (datartl == 1):
            print('RaspberryPi : RTL using Application')
            ChangeMode(vehicle, "RTL")
        else:
            print('RaspberryPi : LAND using Application')
            ChangeMode(vehicle, "LAND")
    if (msg.topic == 'RequestMissionPath'):
        if (json.loads(msg.payload) == 1):
            client.publish("ResponseMissionPath", missionpath)


def connectMqtt(client):
    # The callback for when the client receives a CONNACK response from the server.
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    # The callback for when a PUBLISH message is received from the server.
    client.on_connect = on_connect
    client.on_message = on_message
    # client.username_pw_set("admin", "HN9yx3OtGDZH")
    client.connect(mqttIP)
    client.subscribe("Mission")
    client.subscribe("RequestMissionPath")
    client.subscribe("HoverMove")
    client.subscribe("RTL_LAND")
    dDatathread = threading.Thread(target=publishDroneData, args=(client,))
    dDatathread.start()

    client.loop_forever()


def publishDroneData(client):
    global _FINISH
    global logList
    global flagLastLocation
    global distance
    lastlocation = 0
    while True:
        client.publish("DroneHeartBeat", "1")
        dData = droneData()

        if flagLastLocation == 1:
            distance = get_distance_metres(vehicle.location.global_relative_frame, lastlocation) + distance
        if flagLastLocation == 0:
            lastlocation = vehicle.location.global_relative_frame
            flagLastLocation = flagLastLocation + 1

        location = vehicle.location.global_relative_frame
        lastlocation = location
        dData.Latitude = location.lat
        dData.Longitude = location.lon
        dData.Altitude = location.alt
        dData.NextWp = 0
        dData.Battery = vehicle.battery.voltage
        dData.Velocity = str(vehicle.velocity)
        dData.Heading = vehicle.heading
        dData.Mode = vehicle.mode.name
        dData.inMission = not (_FINISH)
        dData.satellites = vehicle.gps_0.satellites_visible
        dData.Current = vehicle.battery.current
        if flagLastLocation > 0 and dData.Current:
            logList.append([distance, dData.Battery, dData.Current])
        client.publish("DroneData", dData.toJSON())
        time.sleep(1)


class droneData:
    Latitude = 0.000000
    Longitude = 0.000000
    Altitude = 0.000000
    NextWP = 0
    Velocity = 0.000000
    Heading = 0
    Battery = 0.000000
    Mode = 0
    inMission = False
    Satellites = 0
    Current = 0.000000

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)


if True:
    #   cameraControl.setDataModeOn()
    #   time.sleep(5)
    #   getFirstImage(cameraControl.getLastFileName())
    #   cameraControl.stopCamera()
    #   cameraControl.startCamera()
    # vehicle = connect(connection_string, baud=921600, wait_ready=True)
    # vehicle.parameters['WP_YAW_BEHAVIOR'] = 1  # 1 - Face next waypoint
    # ChangeMode(vehicle, "GUIDED")
    print("RaspberryPi : vehicle mode: %s" % VehicleMode)
    connectMqtt(client)
    # gpCam = GoProCamera.GoPro()
    # camera()
    # cameraControl.stopCamera()
# except:
#     # cameraControl.stopCamera()
#     e = sys.exc_info()[0]
#     print(str(e))
#     client.publish("DroneAutomatorReply", str(e))
