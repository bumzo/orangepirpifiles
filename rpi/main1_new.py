from __future__ import print_function
#import urllib2
#from pexpect import pxssh
from array import *
# from serial import Serial
import time
import math
import sys
import threading
import geopy.distance
import argparse
from datetime import datetime
from dronekit import connect, VehicleMode, LocationGlobal, LocationGlobalRelative
from pymavlink import mavutil  # Needed for command message definitions
import time
import math
import paho.mqtt.client as mqtt
import json
import utm
from goprocam import GoProCamera
from goprocam import constants
# import Camera_video_stream
# import cameraControl
#from xiaomi_yi import XiaomiYi

firstImage2Download = 0
count = 0
ncount = 0
_FINISH = True
_hover = False
_lastPoint = False
import argparse
import time

metaData = ''
_isPhoto = True
def getFirstImage(lastImageName):
    global firstImage2Download
    x = lastImageName.split("YDXJ")
    intLastName = int(x[1].split(".")[0])
    firstImage2Download = intLastName + 1
    # intLastName + 1
    print("first image is")
    print(firstImage2Download)


def validateImage():
    return lastImageName


def set_velocity_body(vx, vy, vz):
    """ Remember: vz is positive downward!!!
    http://ardupilot.org/dev/docs/copter-commands-in-guided-mode.html

    Bitmask to indicate which dimensions should be ignored by the vehicle
    (a value of 0b0000000000000000 or 0b0000001000000000 indicates that
    none of the setpoint dimensions should be ignored). Mapping:
    bit 1: x,  bit 2: y,  bit 3: z,
    bit 4: vx, bit 5: vy, bit 6: vz,
    bit 7: ax, bit 8: ay, bit 9:


    """
    msg = vehicle.message_factory.set_position_target_local_ned_encode(
        0,  # time_boot_ms (not used)
        0, 0,  # target system, target component
        mavutil.mavlink.MAV_FRAME_LOCAL_NED,  # frame
        0b0000111111000111,  # type_mask (only speeds enabled)
        0, 0, 0,  # x, y, z positions (not used)
        vx, vy, vz,  # x, y, z velocity in m/s
        0, 0, 0,  # x, y, z acceleration (not supported yet, ignored in GCS_Mavlink)
        0, 0)  # yaw, yaw_rate (not supported yet, ignored in GCS_Mavlink)
    vehicle.send_mavlink(msg)
    vehicle.flush()
def get_distance_metres(aLocation1, aLocation2):
    """
    Returns the ground distance in metres between two LocationGlobal objects.

    This method is an approximation, and will not be accurate over large distances and close to the
    earth's poles. It comes from the ArduPilot test code:
    https://github.com/diydrones/ardupilot/blob/master/Tools/autotest/common.py
    """
    dlat = aLocation2.lat - aLocation1.lat
    dlong = aLocation2.lon - aLocation1.lon
    # return (math.sqrt((dlat*dlat) + (dlong*dlong)) * 1.113195e5)
    return geopy.distance.vincenty((aLocation1.lat, aLocation1.lon), (aLocation2.lat, aLocation2.lon)).m


def goto(targetLocation, previousLocation, centerLocation=None, isFirst=False,isVideoTaken = 0):
    """
    Moves the vehicle to a position dNorth metres North and dEast metres East of the current position.

    The method takes a function pointer argument with a single `dronekit.lib.LocationGlobal` parameter for
    the target position. This allows it to be called with different position-setting commands.
    By default it uses the standard method: dronekit.lib.Vehicle.simple_goto().

    The method reports the distance to target every two seconds.
    """
    global _hover
    currentLocation = vehicle.location.global_relative_frame
    print("Location at the begining")
    print(currentLocation)
    waitCounter = array('d', [0.00, 0.00, 0.00, 0.00, 0.00])
    vehicle.airspeed = 10
    vehicle.groundspeed = 10
    loopCounter = 0
    targetDistance = get_distance_metres(currentLocation, targetLocation)
    const = 180 / math.pi
    s = utm.from_latlon(targetLocation.lat, targetLocation.lon)
    theta = math.acos(s[0] / math.sqrt((s[0] * s[0]) + (s[1] * s[1]))) * const
    if (s[0] < 0 and s[1] > 0):
        theta = 180 - theta
    if (s[0] < 0 and s[1] < 0):
        theta = 180 + theta
    if (s[0] > 0 and s[1] < 0):
        theta = 360 - theta
    #condition_yaw(vehicle, theta)
    #time.sleep(2)
    print(vehicle.mode.name);
    while vehicle.mode.name == "GUIDED":  # Stop action if we are no longer in guided mode.
        if (True):
            vehicle.simple_goto(targetLocation)
            if centerLocation is not None:
                set_roi(centerLocation)
            remainingDistance = get_distance_metres(vehicle.location.global_relative_frame, targetLocation)
            if ((targetDistance - remainingDistance) > 10 and (isFirst == False) and (isVideoTaken == 0)):
                targetDistance = remainingDistance
                camera()
            if remainingDistance <= 0.5:  # Just below target, in case of undershoot.
                client.publish("DroneAutomatorReply", "Reached target")
                break
            time.sleep(1)
            itrCycle = loopCounter % 4
            if (loopCounter > 0 and itrCycle == 0):
                if ((abs(waitCounter[0] - waitCounter[3]) < 0.3) and remainingDistance < 1):
                    diffX = targetLocation.lon - vehicle.location.global_relative_frame.lon
                    diffY = targetLocation.lat - vehicle.location.global_relative_frame.lat
                    set_velocity_body(diffY * 1.113195e5, diffX * 1.113195e5, 0)
                    loopCounter = -1
                    client.publish("DroneAutomatorReply", "Setting velocity for goto")
                    time.sleep(2)
                    client.publish("DroneAutomatorReply", "Breaking as approx target is reached")
                    break

            waitCounter[itrCycle] = remainingDistance
            loopCounter = loopCounter + 1
        else:
            print("hovering")
            set_velocity_body(0, 0, 0);
            time.sleep(1)


def goto_position_target_global_int(aLocation, vz=0):
    """
    Send SET_POSITION_TARGET_GLOBAL_INT command to request the vehicle fly to a specified LocationGlobal.

    For more information see: https://pixhawk.ethz.ch/mavlink/#SET_POSITION_TARGET_GLOBAL_INT

    See the above link for information on the type_mask (0=enable, 1=ignore).
    At time of writing, acceleration and yaw bits are ignored.
    """
    msg = vehicle.message_factory.set_position_target_global_int_encode(
        0,  # time_boot_ms (not used)
        0, 0,  # target system, target component
        mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT_INT,  # frame
        0b0000111111111000,  # type_mask (only speeds enabled)
        aLocation.lat * 1e7,  # lat_int - X Position in WGS84 frame in 1e7 * meters
        aLocation.lon * 1e7,  # lon_int - Y Position in WGS84 frame in 1e7 * meters
        aLocation.alt,
        # alt - Altitude in meters in AMSL altitude, not WGS84 if absolute or relative, above terrain if GLOBAL_TERRAIN_ALT_INT
        0,  # X velocity in NED frame in m/s
        0,  # Y velocity in NED frame in m/s
        0,  # Z velocity in NED frame in m/s
        0, 0, 0,  # afx, afy, afz acceleration (not supported yet, ignored in GCS_Mavlink)
        0, 0)  # yaw, yaw_rate (not supported yet, ignored in GCS_Mavlink)
    # send command to vehicle
    vehicle.send_mavlink(msg)


def advanced_takeoff_globalframe(location, targetAlt=None):
    if targetAlt is not None:
        altitude = float(targetAlt)
        if math.isnan(altitude) or math.isinf(altitude):
            raise ValueError("Altitude was NaN or Infinity. Please provide a real number")
        msg = vehicle.message_factory.command_long_encode(
            0, 0,  # target system, target component
            mavutil.mavlink.MAV_CMD_NAV_TAKEOFF,  # command
            0,  # confirmation
            0,  # Minimum pitch (if airspeed sensor present), desired pitch without sensor [rad]
            0, 0,  # Empty
            0,  # Yaw angle (if magnetometer present), ignored without magnetometer. NaN for unchanged
            location.lat,  # Latitude
            location.lon,  # Longitude
            altitude)  # target altitude in metres
    # send command to vehicle
    vehicle.send_mavlink(msg)


def arm_and_takeoff(aTargetAltitude):
    print("Basic pre-arm checks")
    # Don't let the user try to arm until autopilot is ready
    timeN = 1
    while not vehicle.is_armable:
        print(" Waiting for vehicle to initialise...")
        time.sleep(1)
        timeN = timeN + 1
    #   if timeN >10:#
    # connection_string = '/dev/ttyACM0'
    #   sitl = None
    #   vehicle = connect(connection_string, wait_ready=True)
    #   ChangeMode(vehicle, "GUIDED")
    #   timeN = 1
    print("Arming motors")
 	# Copter should arm in GUIDED mode
    vehicle.mode = VehicleMode("GUIDED")
    vehicle.armed = True
    cntArm = 0
    while not vehicle.armed:
        print(" Waiting for arming...")
        cntArm = cntArm + 1
        time.sleep(1)
        if (cntArm >= 150):
            client.publish("DroneAutomate", "0")
    # sys.exit()

    print("Taking off!")
    location = vehicle.location.global_relative_frame
    print("location: %s" % location)
    k = 0

    while True:
        current_altitude = vehicle.location.global_relative_frame.alt
        print(" Altitude: %f  Desired: %f" % (current_altitude, aTargetAltitude))
        if current_altitude >= aTargetAltitude * 0.95:  # Trigger just below target alt.
            print("Reached target altitude")
            break
        advanced_takeoff_globalframe(location, aTargetAltitude)  # Take off to target altitude
        time.sleep(1)
        later_altitude = vehicle.location.global_relative_frame.alt
        if (abs(later_altitude - current_altitude) < 0.3):
            k = k + 1
        else:
            k = 0
        if k >= 8:
            set_velocity_body(0, 0, -0.125)
            time.sleep(4)


def gotoAlt(aTargetAltitude):
    location = vehicle.location.global_relative_frame
    c = 1
    if (location.alt > aTargetAltitude):
        c = -1
    while location.alt < aTargetAltitude:
        set_velocity_body(0, 0, -0.25 * c)
        time.sleep(1)
        location = vehicle.location.global_relative_frame


def ChangeMode(vehicle, mode):
    if mode == "RTL":
        vehicle.parameters['RTL_ALT'] = 0
    # vehicle.parameters['RTL_ALT_FINAL'] = 0
    while vehicle.mode != VehicleMode(mode):
        print("waiting to set mode to %s..." % mode)
        vehicle.mode = VehicleMode(mode)
        time.sleep(2)
    return True


def condition_yaw(vehicle, heading, relative=False):
    """
    Send MAV_CMD_CONDITION_YAW message to point vehicle at a specified heading (in degrees).
    This method sets an absolute heading by default, but you can set the `relative` parameter
    to `True` to set yaw relative to the current yaw heading.

    By default the yaw of the vehicle will follow the direction of travel. After setting
    the yaw using this function there is no way to return to the default yaw "follow direction
    of travel" behaviour (https://github.com/diydrones/ardupilot/issues/2427)
    """

    if relative:
        is_relative = 1  # yaw relative to direction of travel
    else:
        is_relative = 0  # yaw is an absolute angle
    print('................. %0.f' % heading)
    # create the CONDITION_YAW command using command_long_encode()
    msg = vehicle.message_factory.command_long_encode(
        0, 0,  # target system, target component
        mavutil.mavlink.MAV_CMD_CONDITION_YAW,  # command
        0,  # confirmation
        heading,  # param 1, yaw in degrees
        0,  # param 2, yaw speed deg/s
        1,  # param 3, direction -1 ccw, 1 cw
        is_relative,  # param 4, relative offset 1, absolute angle 0
        0, 0, 0)  # param 5 ~ 7 not used
    # send command to vehicle
    vehicle.send_mavlink(msg)


def set_roi(location):
    # create the MAV_CMD_DO_SET_ROI command
    msg = vehicle.message_factory.command_long_encode(
        0, 0,  # target system, target component
        mavutil.mavlink.MAV_CMD_DO_SET_ROI,  # command
        0,  # confirmation
        0, 0, 0, 0,  # params 1-4
        location.lat,
        location.lon,
        location.alt
    )
    # send command to vehicle
    vehicle.send_mavlink(msg)


def cmdCircular(radius, angularVelocity):
    Vy = angularVelocity * radius
    yawRate = -angularVelocity
    bootTime = 0  # define startTime = time.time() at the begining of code
    msg = vehicle.message_factory.set_position_target_local_ned_encode(
        bootTime,
        0, 0,
        mavutil.mavlink.MAV_FRAME_BODY_OFFSET_NED,
        0b010111000111,
        0, 0, 0,
        0, Vy, 0,
        0, 0, 0,
        0, yawRate)
    vehicle.send_mavlink(msg)
    vehicle.flush()
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
def camera():
    global _isPhoto
    global _lastPoint
    global metaData
    global count
    global ncount
    global firstImage2Download
    # camera = XiaomiYi(ip="192.168.42.1", port=7878, timeout=10)
    print("thread started")
    # stringURL = "https://tyaxphbs.p73.rt3.io/camera.php?imgName="
    # camera.connect()
    # time.sleep(5)
    # camera.connect()
    a = 0
    # camera.stream()
    if (True):
        if _lastPoint == False:
            gpCam = GoProCamera.GoPro()
            gpCam.take_photo()
            print("Clicked")
            #time.sleep(2)
    # print("Video")
    # camera.start_video()
    else:
        if _lastPoint == False or a < 10:
            client.publish("DroneAutomatorReply", "Clicked a photo")
            location = vehicle.location.global_relative_frame
            cameraControl.takePhoto()
            print("taking photo")
            count = count + 1
            ncount = count
            imgtoD = str(firstImage2Download)
            print(imgtoD)
            if (len(imgtoD) == 3):
                imgtoD = "0" + imgtoD
            imgtoD = "YDXJ" + imgtoD + ".jpg"
            # while validateImage()!=imgtoD:
            client.publish("DroneAutomatorReply", "Validated Photo")
            print("insta")
            metaData = metaData + (imgtoD + "," + str(location.lat) + "," + str(location.lon) + "," + str(
                location.alt) + "," + str(vehicle.attitude.pitch) + "," + str(vehicle.attitude.roll) + "," + str(
                vehicle.attitude.yaw) + "\n")
            print(metaData)
            firstImage2Download = firstImage2Download + 1
            a = a + 1
def perform_goto(data):
    #   cameraThread = threading.Thread(target=camera)
    #        cameraThread.start()
    global _hover
    global _lastPoint
    global metaData
    _lastPoint = False
    #   arm_and_takeoff(10)
    strt = 0
    for i in data:
        if strt == 0:
            arm_and_takeoff(i[2])
        goto(LocationGlobalRelative(i[0], i[1], i[2]), LocationGlobalRelative(i[0], i[1], i[2]),None, True if strt==0 else False,i[3])
        if strt == 0:
            vehicle.parameters['WP_YAW_BEHAVIOR'] = 0
            #cameraThread = threading.Thread(target=camera)
            #cameraThread.start()
            strt = 1
        if (False):
            _hover = True
            _lastPoint = False
            cameraThread = threading.Thread(target=camera)
            cameraThread.start()
            while False:
                print("hovering")
                set_velocity_body(0, 0, 0)
                time.sleep(1)
        # _lastPoint =True
        else:
            p = i[3]
            while (False):
                print("hovering")
                set_velocity_body(0, 0, 0)
                time.sleep(1)
                p = p - 1
    _lastPoint = True
    client.publish("DroneAutomatorReply", "SavingFile")
    file = open(str(int(time.time())) + 'Finalmetadata.txt', 'w+')
    file.write(metaData)
    file.close()
    # ChangeMode(vehicle, "RTL")
    client.subscribe("HomePos")
    client.publish("RequestHomePos", "1")
    time.sleep(20)
    ChangeMode(vehicle, "RTL")
    # Close vehicle object before exiting script
    print("Close vehicle object")

    # Shut down simulator if it was started.
    if sitl is not None:
        sitl.stop()
    _FINISH = False
    print("Completed")


def on_message(client, userdata, msg):
    global _hover
    global _isPhoto
    client.publish("DroneAutomatorReply", msg.topic + " " + str(msg.payload))
    print("1")
    print(msg.topic)
    msg.payload = msg.payload.decode("utf-8")
    print(msg.payload)
    if (msg.topic == 'Mission'):
        data = json.loads(msg.payload)
        print("2")
        print(str(data))
        ChangeMode(vehicle, "GUIDED")
        try:
            pthread = threading.Thread(target=perform_goto, args=(data,))
            pthread.start()
        except:  # catch *all* exceptions
            e = sys.exc_info()[0]
            client.publish("DroneAutomatorReply", str(e))

        print("hello")
    if (msg.topic == "HomePos"):
        my_list = data.split(",")
        a = vehicle.location.global_frame
        a.lat = float(my_list[0])
        a.lon = float(my_list[1])

        msg = vehicle.message_factory.set_home_position_encode(
            0,  # System ID maybe
            a.lat * 1.0e7,  # lat - int32_t Latitude (WGS84), in degrees \* 1E7
            a.lon * 1.0e7,  # lon - int32_t Longitude (WGS84), in degrees \* 1E7
            a.alt - vehicle.location.global_relative_frame.alt,
            # alt - int32_t  Altitude (AMSL), in meters \* 1000 (positive for up)
            0, 0, 0, [1, 1, 1, 1], 0, 0, 0  # Remaining parameters not supported by ardupilot.
        )
        ChangeMode(vehicle, "RTL")
        unsubscribe(msg.topic)
    if (msg.topic == "PhotoVideo"):
        if (msg.payload == 0):
            _isPhoto = False
        else:
            _isPhoto = True
    if (msg.topic == "HoverMove"):
        if (msg.payload == 0):
            print("here")
            _hover = True
        else:
            _hover = False
    if (msg.topic == 'RTL_LAND'):
        if (msg.payload == 1):
            print('RTL using Application')
            ChangeMode(vehicle, "RTL")
        else:
            print('LAND using Application')
            ChangeMode(vehicle, "LAND")
def connectMqtt(client):
    # The callback for when the client receives a CONNACK response from the server.
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    # The callback for when a PUBLISH message is received from the server.
    client.on_connect = on_connect
    client.on_message = on_message
    # client.username_pw_set("admin", "HN9yx3OtGDZH")
    client.connect("192.168.0.1")
    client.subscribe("Mission")
    client.subscribe("HoverMove")
    client.subscribe("RTL_LAND")
    dDatathread = threading.Thread(target=publishDroneData, args=(client,))
    dDatathread.start()

    client.loop_forever()


def publishDroneData(client):
    global _FINISH
    while True:
        client.publish("DroneHeartBeat", "1")
        location = vehicle.location.global_relative_frame
        dData = droneData()
        dData.Latitude = location.lat
        dData.Longitude = location.lon
        dData.Altitude = location.alt
        dData.NextWp = 0
        dData.Battery = vehicle.battery.voltage
        dData.Velocty = vehicle.velocity
        dData.Heading = vehicle.heading
        client.publish("DroneData", dData.toJSON())
        time.sleep(1)


class droneData:
    Latitude = 0.000000
    Longitude = 0.000000
    Altitude = 0.000000
    NextWP = 0
    Velocty = 0.000000
    Heading = 0
    Battery = 0.000000

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)
client = mqtt.Client("P1")
# .write(str(99) + '\n')
connection_string = '/dev/ttyAMA0'
sitl = None


if True:
    #   cameraControl.setDataModeOn()
    #   time.sleep(5)
    #   getFirstImage(cameraControl.getLastFileName())
    #   cameraControl.stopCamera()
    #   cameraControl.startCamera()
    vehicle = connect(connection_string, baud=921600, wait_ready=True)
    vehicle.parameters['WP_YAW_BEHAVIOR'] = 0
    # ChangeMode(vehicle, "GUIDED")
    print("vehicle mode: %s" % VehicleMode)
    connectMqtt(client)
    cameraControl.stopCamera()
# except:
#     # cameraControl.stopCamera()
#     e = sys.exc_info()[0]
#     print(str(e))
#     client.publish("DroneAutomatorReply", str(e))
